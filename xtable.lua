local setmetatable = setmetatable
local error = error
local newproxy = newproxy

-- -- -- -- --

local tables = {}

local mt = {
  __index = function(t, k)
    local hidden = tables[t]
    if k == nil and hidden.hasnilkey then
      return hidden.nilkey
    elseif k ~= k and hidden.hasnankey then
      return hidden.nankey
    elseif hidden.keys[k] then
      return hidden.values[k]
    elseif hidden.errormissing then
      error("No such key")
    end
  end,
  __newindex = function(t, k, v)
    local hidden = tables[t]
    if hidden.readonly then
      error("Read-only")
    elseif k == nil then
      hidden.hasnilkey = true
      hidden.nilkey = v
    elseif k ~= k then
      hidden.hasnankey = true
      hidden.nankey = v
    else
      hidden.keys[k] = true
      hidden.values[k] = v
    end
  end,
  __len = function(t)
    return #tables[t].keys
  end
}

local baseproxy = (function()
  if newproxy then
    local p = newproxy(true)
    getmetatable(p).__index = mt.__index
    getmetatable(p).__newindex = mt.__newindex
    getmetatable(p).__len = mt.__len
    return p
  end
end)()

local ctor = newproxy and function(prop)
  local t = newproxy(baseproxy)
  tables[t] = {keys={}, values={}, errormissing=prop.errormissing, readonly=prop.readonly}
  return t
end or function(prop)
  local t = {} 
  tables[t] = {keys={}, values={}, errormissing=prop.errormissing, readonly=prop.readonly}
  setmetatable(t, mt)
  return t
end
  

local function new()
  return ctor({})
end

local function del(t, k)
  local hidden = tables[t]
  if hidden.readonly then
    error("Read-only")
  elseif k == nil then
    hidden.hasnilkey = nil
    hidden.nilkey = nil
  elseif k ~= k then
    hidden.hasnankey = nil
    hidden.nankey = nil
  else
    hidden.keys[k] = nil
    hidden.values[k] = nil
  end
end

local function has(t, k)
  local hidden = tables[t]
  local v
  if k == nil then
    v = hidden.hasnilkey
  elseif k ~= k then
    v = hidden.hasnankey
  else
    v = hidden.keys[k]
  end
  return v or false
end

return {new = new, del = del, has = has, ctor = ctor}
