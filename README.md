xtable - Boring table module
============================

Usage:

    local xtable = require "xtable"
    local has, del = xtable.has, xtable.del -- for convenience
    local t = xtable.new() -- create a new xtable
    t[nil] = 1 -- nil key
    t[0/0] = 2 -- nan key
    t[1] = nil -- nil value
    assert(has(t,1))
    assert(#t == 1)
    del(t,1) -- removing a key is not with `t[k] = nil`
    assert(#t == 0)
    t = nil
    
    local t = xtable.ctor({errormissing=true, readonly=true}) -- useless table
    assert(not pcall(function() t[1] = 1 end)) -- read-only
    assert(not pcall(function() return t[1] end)) -- error on missing keys
